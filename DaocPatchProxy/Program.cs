
namespace DaocPatchProxy
{
	public class Program
	{
		/// <summary>
		/// One of the two IP addresses associated with the real
		/// patch.daoc.broadsword.com. You can verify with an online
		/// DNS lookup service. The trailing slash is required.
		/// </summary>
		private const string PATCH_SERVER_ADDRESS = "http://107.23.154.34/";

		public static void Main(string[] args)
		{
			var builder = WebApplication.CreateBuilder(args);
			builder.Services.AddSingleton(sp =>
				new HttpClient() { BaseAddress = new Uri(PATCH_SERVER_ADDRESS) });
			builder.Services.AddControllers();
			var app = builder.Build();
			app.MapControllers();
			app.Run();
		}
	}
}
