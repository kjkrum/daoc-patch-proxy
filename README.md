# DAoC Patch Proxy

A local proxy for `http://patch.daoc.broadsword.com`.

## What problem does this solve?
On November 28, 2023, the official DAoC servers were patched to version 1.128b. I attempted to patch the following day, but my patcher kept timing out. Looking at the patcher logs, I could see that it was failing to load `http://patch.daoc.broadsword.com:1380/daocpatch/live/patcher/manifest/patcher.prod.sig`. I tried loading this URL in a browser and got the same result.

Acting on a hunch, I visited `http://patch.daoc.broadsword.com` &mdash; note the unspecified port number. This displays the Apache test page! Going down this rabbit hole, I tried loading `http://patch.daoc.broadsword.com/daocpatch/live/patcher/manifest/patcher.prod.sig` &mdash; again note the unspecified port number. This URL works!

The URL of the patch server is specified in `patch.cfg`, so in theory I should have been able to remove the port number there and everything should have worked. But either the patcher hardcodes port 1380 for some files, or the manifests it downloads contain some absolute URLs with that port number.

If you can't patch, [this link](http://patch.daoc.broadsword.com:1380) times out, and [this link](http://patch.daoc.broadsword.com) displays the Apache test page, this solution is for you. 

## What this program does
This is an ASP.NET Core web app with a single controller action that listens for HTTP requests on port 1380 and downloads the corresponding file from the real patch server using port 80.

First, you need to trick the patcher into connecting to your local machine instead of the real patch server. Do this by editing your hosts file, which in Windows 10 and 11 is `C:\Windows\system32\drivers\etc\hosts`. It's just a text file. Add the following line:

```
127.0.0.1	patch.daoc.broadsword.com
```

Next, install and run this program. Clone this git repo if you know how to do that, or just download and unzip it. If you don't have it already, you'll need to install the [.NET 8 SDK](https://dotnet.microsoft.com/en-us/download/dotnet/8.0). Then open a command prompt, `cd` to the directory containing `DaocPatchProxy.csproj`, and type `dotnet run`.

Finally, run `camelot.exe` to patch.
