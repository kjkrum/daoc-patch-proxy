using Microsoft.AspNetCore.Mvc;

namespace DaocPatchProxy.Controllers
{
	public class DownloadController(HttpClient client, ILogger<DownloadController> log) : ControllerBase
	{
		private readonly HttpClient _client = client;
		private readonly ILogger<DownloadController> _log = log;

		[Route("/{*relativePath}")]
		public async Task<IActionResult> Get(string relativePath)
		{
			// download the requested file from the real patch server and return it
			_log.LogInformation("{%s}", relativePath);
			var content = await _client.GetByteArrayAsync(relativePath);
			return new FileContentResult(content, "application/octet-stream");
		}
	}
}
